#include "hzpch.h"
#include "MacOSInput.h"

#include "Hazel/Application.h"
#include <GLFW/glfw3.h>

namespace Hazel {

  Input* Input::s_Instance = new MacOSInput();
  
  bool MacOSInput::IsKeyPressedImpl(int keycode)
  {
    const auto& window = static_cast<GLFWwindow*>(Application::Get().GetWindow().GetNativeWindow());
    auto state = glfwGetKey(window, keycode);
    return state == GLFW_PRESS || state == GLFW_REPEAT;
  }

  bool MacOSInput::IsMouseButtonPressedImpl(int button)
  {
    const auto& window = static_cast<GLFWwindow*>(Application::Get().GetWindow().GetNativeWindow());
    auto state = glfwGetMouseButton(window, button);
    return state == GLFW_PRESS;
  }

  float MacOSInput::GetMouseXImpl()
  {
    auto[x, y] = GetMousePositionImpl();
    return x;
  }
  
  float MacOSInput::GetMouseYImpl()
  {
    auto[x, y] = GetMousePositionImpl();
    return y;
  }

  std::pair<float, float> MacOSInput::GetMousePositionImpl()
  {
    const auto& window = static_cast<GLFWwindow*>(Application::Get().GetWindow().GetNativeWindow());
    double xPos, yPos;
    glfwGetCursorPos(window, &xPos, &yPos);

    return {(float)xPos, (float)yPos};
  }


}